package com.mall.service;

import com.mall.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Desmond on 16/6/12.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})

public class TestUserService
{
    @Autowired
    private UserService userService;

    @Test
    public void hasMatchUser()
    {
        boolean b1 = userService.hasMatchUser("desmond", "warcraft123");
        boolean b2 = userService.hasMatchUser("desmond", "1111");
        assertTrue(b1);
        assertTrue(!b2);
    }

    @Test
    public void findUserByUserName()
    {
        User user = userService.findUserByUserName("desmond");
        assertEquals(user.getUserName(),"desmond");
    }



}
