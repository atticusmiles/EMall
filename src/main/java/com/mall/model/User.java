package com.mall.model;

import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.Date;

/**
 * Created by Desmond on 16/6/12.
 */
public class User implements Serializable
{
    private int userId;

    private String userName;

    private String password;

    private String lastIp;

    private Date lastDate;

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getLastIp()
    {
        return lastIp;
    }

    public void setLastIp(String lastIp)
    {
        this.lastIp = lastIp;
    }

    public Date getLastDate()
    {
        return lastDate;
    }

    public void setLastDate(Date lastDate)
    {
        this.lastDate = lastDate;
    }
}
